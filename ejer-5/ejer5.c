#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int getRandomNumber(int min, int max){
    return (rand() % (max - min + 1)) + min;
}

void bubble_sort(int *list, int n){
  long c, d, t;

  for (c = 0 ; c < n - 1; c++) {
    for (d = 0 ; d < n - c - 1; d++) {
      if (list[d] > list[d+1]) {
        /* Swapping */
        t         = list[d];
        list[d]   = list[d+1];
        list[d+1] = t;
      }
    }
  }
}

int main(){

    int cantidad;
    printf("Ingrese la cantidad de posiciones que quiere reservar: ");
    scanf("%d",&cantidad);
    
    int numeros[cantidad];
    printf("\n\n");
    printf("Tabla de Valores Original\n");
    for (int i = 0; i < cantidad; i++){
        numeros[i] = getRandomNumber(0, 100);
        printf("%d\t", numeros[i]);
    }
    
    bubble_sort(numeros, cantidad);

    printf("\n\n");
    printf("Tabla de Valores Ordenandos\n");
    for (int i = 0; i < cantidad; i++){
        printf("%d\t", numeros[i]);
    }
    printf("\n");
    
    return 0;
}
