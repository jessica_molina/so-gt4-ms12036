#include <stdio.h>
#include <stdlib.h>
#include <math.h>
int main(){

    int grado;              
    double x;               
    double *coeficientes;   

    printf("Ingrese el grado del polinomio: ");
    scanf("%d/n",&grado);
    printf("\n");
    coeficientes = malloc((grado + 1) * sizeof(double));
    for (int i = 0; i < (grado + 1); i++){
        printf("Ingrese el Coeficiente A%d: ", i);
        scanf("%lf",&coeficientes[i]);
    }
    printf("\n");
    printf("Ingrese el valor de X a evaluar: ");
    scanf("%lf",&x);
    printf("\n");
    double resultado = 0.0;
    printf("Funcion a evaluar y resultado P(x):\nP(%3.2f) = ", x);
    for (int i = 0; i < (grado + 1); i++){
        printf("(%3.2f).(X^%d)", coeficientes[i], i);
        resultado += (coeficientes[i])*(pow(x, i));
        if(i != grado ){
            printf(" + ");
        }
    }
    printf(" = %3.2f\n", resultado);

    return 0;
}
